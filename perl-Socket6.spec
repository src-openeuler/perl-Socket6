Name:           perl-Socket6
Version:        0.29
Release:        3
Summary:        A getaddrinfo/getnameinfo support module
License:        BSD-3-Clause
URL:            https://metacpan.org/release/Socket6
Source0:        https://cpan.metacpan.org/authors/id/U/UM/UMEMOTO/Socket6-%{version}.tar.gz

BuildRequires:  gcc coreutils findutils make perl-interpreter perl-devel perl-generators
BuildRequires:  perl(Config) perl(ExtUtils::MakeMaker) perl(base) perl(Carp)
BuildRequires:  perl(DynaLoader) perl(Exporter) perl(strict) perl(vars)
BuildRequires:  perl(Socket) perl(Test)

%{?perl_default_filter}

%description
This module supports getaddrinfo() and getnameinfo() to intend to
enable protocol independent programming.
If your environment supports IPv6, IPv6 related defines such as
AF_INET6 are included.

%package_help

%prep
%autosetup -n Socket6-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{optflags}"
%make_build

%install
make pure_install DESTDIR=%{buildroot}
%{_fixperms} %{buildroot}

%check
make test

%files
%{perl_vendorarch}/Socket6.pm
%{perl_vendorarch}/auto/Socket6/

%files          help
%doc ChangeLog README gailookup.pl
%{_mandir}/man3/Socket6.3*

%changelog
* Sun Jan 19 2025 Funda Wang <fundawang@yeah.net> - 0.29-3
- drop useless perl(:MODULE_COMPAT) requirement

* Sat Dec 24 2022 wanglimin <wanglimin@xfusion.com> - 0.29-2
- Fix date error on 028-10

* Tue Jun 14 2022 SimpleUpdate Robot <tc@openeuler.org> - 0.29-1
- Upgrade to version 0.29

* Wed Jun 02 2021 zhaoyao<zhaoyao32@huawei.com> - 0.28-10
- fixs faileds: /bin/sh: gcc: command not found.

* Fri Nov 29 2019 Jiangping Hu <hujiangping@huawei.com> - 0.28.9
- Package init
